import {RouterModule, Routes} from "@angular/router";
import {PagesComponent} from "./pages.component";
import {UsersComponent} from "./users/users.component";
import {NgModule} from "@angular/core";
import {FeedComponent} from "./feed/feed.component";
import {MagazineComponent} from "./magazine/magazine.component";

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    { path: 'users', component: UsersComponent},
    { path: 'feed', component: FeedComponent},
    { path: 'magazine', component: MagazineComponent},
    {path: '', redirectTo: 'users', pathMatch: 'full'},
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRouter {
}
