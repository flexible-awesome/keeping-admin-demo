import {AfterViewInit, Component, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {CommonService} from "../../services/common.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {HttpService} from "../../services/http.service";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit, AfterViewInit {

  dataSource = new MatTableDataSource<any>([]);

  constructor(
    private commonService: CommonService,
    private httpService: HttpService,
    private httpClient: HttpClient
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.findAllFeeds().subscribe((feed: any) => {
      console.log('feed : ', feed);
      console.log('/// feed finding complete /// ');
    })
  }

  /**
   * 피드 전체 조회
   * */
  findAllFeeds(page?: number, pageSize?: number): Observable<any> {
    page = page ? page : 1;
    pageSize = pageSize ? pageSize : 10;
    console.log('///  feed finding ... ///')
    const url = `${environment.serverAddress}/public/feed?page=${page}&pageSize=${pageSize}`;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    return this.httpClient.get(url, {headers});
  }

}
