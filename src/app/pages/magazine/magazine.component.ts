import {AfterViewInit, Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-magazine',
  templateUrl: './magazine.component.html',
  styleUrls: ['./magazine.component.scss']
})
export class MagazineComponent implements OnInit, AfterViewInit {

  list: any[] = [
    {id: 'id1'},
    {id: 'id2'},
    {id: 'id3'},
    {id: 'id4'},
    {id: 'id5'},
    {id: 'id6'},
  ];

  /* body */
  data = {
    "id": "",
    "password": ""
  };

  constructor(
    private httpClient: HttpClient
  ) { }

  addData(): void {
    this.list.push({id: 'asdasd'});
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {

  }

}
