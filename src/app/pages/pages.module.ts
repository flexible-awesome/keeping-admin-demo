import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersComponent} from './users/users.component';
import {FeedComponent} from './feed/feed.component';
import {MagazineComponent} from './magazine/magazine.component';
import {PagesRouter} from "./pages-router";
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";

@NgModule({
  declarations: [
    UsersComponent,
    FeedComponent,
    MagazineComponent
  ],
  imports: [
    CommonModule,
    PagesRouter,
    MatTableModule,
    MatSortModule
  ],
  providers: [
  ]
})
export class PagesModule { }
