import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../../domain/Users';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from "@angular/material/sort";
import {CommonService} from "../../services/common.service";
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {HttpService} from "../../services/http.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, AfterViewInit {

  dataSource = new MatTableDataSource<any>([]);

  @ViewChild(MatSort) sort: MatSort | undefined;

  displayedColumns: string[] = [
    'name',
    'nickname',
    'birthDate',
    'email',
    'action',
    'view'
  ];

  chooseUsers: User | undefined;

  @ViewChild(MatSort) matSort: MatSort = new MatSort;

  userList: User[] = [];
  dataset: any = {};

  constructor(
    private commonService: CommonService,
    private httpService: HttpService,
    private httpClient: HttpClient,
  ) { }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.matSort;

    this.getAllByUser().subscribe((data: any) => {
      console.log('getAllByUser() data : ', data);
      // this.dataSource.data = data;
      this.dataset = data;

      if (data.dataset.length > 0) {
        this.userList = data.dataset[0];
        this.dataSource.data = this.userList;
      }

    });
  }

  findAllByUser(): Observable<User[]> {
    let page: number = 1;
    let pageSize: number = 10;
    let url = `/public/feed?page=${page}&pageSize=${pageSize}`;

    console.log('-------> ', `${environment.serverAddress}${url}`);

    return this.httpClient.post<User[]>(`${environment.serverAddress}${url}`, {});
  }

  getAllByUser(): Observable<any> {
    let page: number = 1;
    let pageSize: number = 10;
    let url = `/users/getAll`;

    return this.httpClient.get<any>(`${environment.serverAddress}${url}`);
  }

  chooseUser(user: User): void {
    this.chooseUsers = user;
  }


  generateSampleUsers(generateCount: number): User[] {
    let dataSet: User[] = [];

    for (let i: number = 0; i < generateCount; i++) {
      let user: User = new User(
        `sample${i}`,
        `flexible${i}`,
        `사용자${i}`,
        `닉네임${i}`,
        1 + i,
        '성별',
        '2021-12-25',
        `test-${i}@flexible.link`,
        true,
        '010-1234-1234',
        true,
        'red',
        'empty',
        true,
        true,
        true,
        true,
        new Array<File>(),
        new Array<File>(),
        `'token-'${i}`,
        `refresh-${i}`,
        `custom-${i}`,
      );
      dataSet.push(user);
    }

    return dataSet;
  }

  waitingMessage(): void {
    alert('준비중입니다.');
  }


}
