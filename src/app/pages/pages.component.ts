import {AfterViewInit, Component, OnInit} from '@angular/core';
import {CommonService} from "../services/common.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit, AfterViewInit {

  menus: any = [
    { title: '사용자 관리', path: 'users' },
    { title: '피드 관리', path: 'feed' },
    { title: '매거진 관리', path: 'magazine' },
  ];

  userName: string = 'Flexible';

  today: Date = new Date();
  weekDay: string = 'Sunday';

  constructor(
    private commonService: CommonService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.initWeekDays();
  }

  ngAfterViewInit(): void {
    this.initWeekDays();
  }

  initWeekDays(): void {
    const weekDay: string[] = [
      '일요일',
      '월요일',
      '화요일',
      '수요일',
      '목요일',
      '금요일',
      '토요일',
    ];

    this.weekDay = weekDay[this.today.getDay()];
  }

  logout(): void {
    this.commonService.removeSessionItem('user');
    this.router.navigateByUrl('login').then(() => {});
  }

}
