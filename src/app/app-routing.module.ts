import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {PagesComponent} from "./pages/pages.component";
import {NotPoundComponent} from "./not-pound/not-pound.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'pages', loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)},
  { path: 'pages', component: PagesComponent},
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'not-pound', component: NotPoundComponent},
  { path: '**', redirectTo: 'not-pound' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, scrollPositionRestoration: 'top', })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
