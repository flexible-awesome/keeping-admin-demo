import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-not-pound',
  templateUrl: './not-pound.component.html',
  styleUrls: ['./not-pound.component.scss']
})
export class NotPoundComponent implements OnInit, AfterViewInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.moveLoginPage();
  }

  moveLoginPage(): void {
    this.router
      .navigate(['login'])
      .then(() => {
        alert('잘못된 접근입니다.');
      });
  }



}
