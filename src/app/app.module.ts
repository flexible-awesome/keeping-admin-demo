import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {PagesComponent} from './pages/pages.component';
import {NotPoundComponent} from './not-pound/not-pound.component';
import {COMPOSITION_BUFFER_MODE, ReactiveFormsModule} from "@angular/forms";
import {HttpService} from "./services/http.service";
import {CommonService} from "./services/common.service";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PagesComponent,
    NotPoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    { provide: COMPOSITION_BUFFER_MODE, useValue: false },
    CommonService,
    HttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
