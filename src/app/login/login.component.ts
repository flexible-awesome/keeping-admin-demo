import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CommonService} from "../services/common.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  tempUser: any = {
    userId: 'admin',
    password: 'admin',
  };

  loginForm: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private commonService: CommonService,) {
    this.loginForm = this.formBuilder.group({
      userId: ['', [Validators.required,]],
      password: ['', [Validators.required,]],
    });
  }

  ngOnInit(): void {
  }

  login(): void {
    this.loginCheck();
  }

  loginCheck(): void {
    let formData: any = this.loginForm.getRawValue();

    console.log('formData : ', formData);

    if (formData.userId === this.tempUser.userId
      && formData.password === this.tempUser.password) {
      this.movePages();
    } else {
      alert('비밀번호가 일치하지 않습니다.');
    }
  }

  movePages(): void {
    this.router
      .navigate(['pages/users'])
      .then(() => {
        this.commonService.setSessionItem('user', this.loginForm.getRawValue());
      });
  }

}
