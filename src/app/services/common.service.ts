import {Injectable} from '@angular/core';

import * as CryptoJS from 'crypto-js';

@Injectable()
export class CommonService {

  constructor() {
  }

  setSessionItem(key: string, item: any) {
    let itemStr = JSON.stringify(item);
    itemStr = this._utf8_encode(itemStr);
    let plainText = 'FLEXIBLE';
    itemStr = CryptoJS.AES.encrypt(itemStr, plainText).toString();
    sessionStorage.setItem(key, itemStr);
  }

  getSessionItem(key: string) {
    // 마스터 2에서 편집
    // 마스터 2에서 편집

    let itemStr: any = sessionStorage.getItem(key);
    let item = null;
    if (itemStr) {
      let plainText = 'FLEXIBLE';
      itemStr = CryptoJS.AES.decrypt(itemStr, plainText).toString(CryptoJS.enc.Utf8);
      itemStr = this._utf8_decode(itemStr)
      item = JSON.parse(itemStr);
    }
    return item;
  }

  removeSessionItem(key: string): void {
    sessionStorage.removeItem(key);
  }

  _utf8_encode(string: string) {
    string = string.replace(/\r\n/g, "\n");
    let utftext = '';

    for (let n = 0; n < string.length; n++) {

      let c = string.charCodeAt(n);

      if (c < 128) {
        utftext += String.fromCharCode(c);
      } else if ((c > 127) && (c < 2048)) {
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      } else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }

    }

    return utftext;
  }

  // private method for UTF-8 decoding
  _utf8_decode(utftext: string) {
    var string = "";
    var i = 0;
    var c = 0;
    var c1 = 0;
    var c2 = 0;
    var c3 = 0;

    while (i < utftext.length) {

      c = utftext.charCodeAt(i);

      if (c < 128) {
        string += String.fromCharCode(c);
        i++;
      } else if ((c > 191) && (c < 224)) {
        c2 = utftext.charCodeAt(i + 1);
        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
        i += 2;
      } else {
        c2 = utftext.charCodeAt(i + 1);
        c3 = utftext.charCodeAt(i + 2);
        string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
        i += 3;
      }

    }

    return string;
  }
}
