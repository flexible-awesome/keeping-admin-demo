export const environment = {
  production: true,
  siteName: '키핑-관리자',
  serviceUrl: 'http://localhost:8080',
  serverAddress: 'https://dev.keeping.link:3000/latest',
};
