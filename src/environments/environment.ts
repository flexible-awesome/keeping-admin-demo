// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  siteName: '키핑-관리자',
  serviceUrl: 'http://localhost:3001',
  serverAddress: 'http://localhost:3000/latest',
  wss: 'wss://sgwpc3hmp9.execute-api.ap-northeast-2.amazonaws.com/prod',
  firebase: {
    apiKey: 'AIzaSyDYjkQCA1CEqOhX0DUetR4H9f5gJqf2rsQ',
    authDomain: 'keeping-7d8fc.firebaseapp.com',
    projectId: 'keeping-7d8fc',
    storageBucket: 'keeping-7d8fc.appspot.com',
    messagingSenderId: '1008125577199',
    appId: '1:1008125577199:web:1bb9f72c18fc8967cf981a',
    measurementId: 'G-9R9CY4QP6P',
  },
  kakao: {
    appKey: {
      javascript: '10e949a1778f8fce2059bb4cea294c1a',
    },
  },
  naver: {
    clientId: 'ee2BRpS6JiR0v9U176nS',
    redirectUri: 'https://dev.keeping.link/auth/callback',
  },
  recaptchaSiteKey: '',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
